# coffeedotpuppet

Visualize and manipulate Puppet dotfiles in your browser.

## Author
Nathan Valentine - nrvale0@gmail.com

## Support
coffeedotpuppet - https://github.com/nrvale0/coffeedotpuppet
